-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 08 Des 2018 pada 09.26
-- Versi Server: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_elektronik_master`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `invoices`
--

CREATE TABLE `invoices` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `due_date` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` enum('paid','confirmed','unpaid','canceled','expired') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `invoices`
--

INSERT INTO `invoices` (`id`, `date`, `due_date`, `user_id`, `status`) VALUES
(8, '2018-11-19 14:04:40', '2018-11-20 14:04:40', 1, 'confirmed'),
(9, '2018-11-19 15:09:03', '2018-11-20 15:09:03', 2, 'unpaid'),
(10, '2018-11-19 15:27:27', '2018-11-20 15:27:27', 1, 'confirmed'),
(11, '2018-11-19 15:28:14', '2018-11-20 15:28:14', 1, 'confirmed'),
(12, '2018-11-19 15:30:39', '2018-11-20 15:30:39', 1, 'confirmed'),
(13, '2018-11-19 15:31:50', '2018-11-20 15:31:50', 1, 'unpaid'),
(14, '2018-11-19 15:32:08', '2018-11-20 15:32:08', 1, 'unpaid'),
(15, '2018-11-19 15:32:51', '2018-11-20 15:32:51', 1, 'confirmed'),
(16, '2018-11-19 15:40:21', '2018-11-20 15:40:21', 1, 'unpaid'),
(17, '2018-11-19 15:48:19', '2018-11-20 15:48:19', 1, 'confirmed'),
(18, '2018-11-20 10:13:47', '2018-11-21 10:13:47', 1, 'unpaid'),
(74, '2018-11-21 14:49:46', '2018-11-22 14:49:46', 1, 'unpaid'),
(75, '2018-11-21 14:58:41', '2018-11-22 14:58:41', 1, 'unpaid'),
(76, '2018-11-21 15:01:41', '2018-11-22 15:01:41', 1, 'unpaid'),
(77, '2018-11-21 15:01:45', '2018-11-22 15:01:45', 1, 'unpaid'),
(78, '2018-11-21 15:08:20', '2018-11-22 15:08:20', 1, 'confirmed'),
(79, '2018-11-23 16:21:25', '2018-11-24 16:21:25', 1, 'confirmed'),
(80, '2018-11-23 16:29:57', '2018-11-24 16:29:57', 1, 'confirmed'),
(81, '2018-11-24 11:11:46', '2018-11-25 11:11:46', 2, 'unpaid'),
(82, '2018-11-24 11:12:51', '2018-11-25 11:12:51', 2, 'unpaid'),
(83, '2018-11-25 07:34:12', '2018-11-26 07:34:12', 2, 'unpaid'),
(84, '2018-11-25 07:37:05', '2018-11-26 07:37:05', 2, 'confirmed'),
(85, '2018-11-29 11:43:39', '2018-11-30 11:43:39', 2, 'unpaid'),
(86, '2018-11-29 11:45:38', '2018-11-30 11:45:38', 2, 'unpaid'),
(87, '2018-11-29 11:48:04', '2018-11-30 11:48:04', 2, 'confirmed'),
(88, '2018-11-29 14:22:45', '2018-11-30 14:22:45', 2, 'unpaid'),
(89, '2018-12-04 15:05:23', '2018-12-05 15:05:23', 2, 'unpaid'),
(90, '2018-12-06 13:29:09', '2018-12-07 13:29:09', 1, 'confirmed'),
(91, '2018-12-06 14:24:15', '2018-12-07 14:24:15', 2, 'unpaid'),
(92, '2018-12-06 14:36:20', '2018-12-07 14:36:20', 2, 'unpaid'),
(93, '2018-12-06 14:41:32', '2018-12-07 14:41:32', 2, 'unpaid'),
(94, '2018-12-06 15:10:30', '2018-12-07 15:10:30', 2, 'unpaid'),
(95, '2018-12-06 15:14:15', '2018-12-07 15:14:15', 2, 'unpaid'),
(96, '2018-12-07 04:58:38', '2018-12-08 04:58:38', 2, 'unpaid'),
(97, '2018-12-07 05:00:31', '2018-12-08 05:00:31', 2, 'unpaid'),
(98, '2018-12-07 05:00:32', '2018-12-08 05:00:32', 2, 'unpaid'),
(99, '2018-12-07 05:02:02', '2018-12-08 05:02:02', 2, 'unpaid'),
(100, '2018-12-07 05:03:19', '2018-12-08 05:03:19', 2, 'unpaid'),
(101, '2018-12-07 13:57:56', '2018-12-08 13:57:56', 2, 'unpaid'),
(102, '2018-12-07 14:08:01', '2018-12-08 14:08:01', 2, 'unpaid'),
(103, '2018-12-08 06:38:15', '2018-12-09 06:38:15', 1, 'unpaid'),
(104, '2018-12-08 06:49:37', '2018-12-09 06:49:37', 1, 'unpaid');

-- --------------------------------------------------------

--
-- Struktur dari tabel `login_session`
--

CREATE TABLE `login_session` (
  `id_login` bigint(20) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `login_session`
--

INSERT INTO `login_session` (`id_login`, `username`, `password`, `level`) VALUES
(1, 'admin', 'admin', 1),
(2, 'member', 'member', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `options` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `orders`
--

INSERT INTO `orders` (`id`, `invoice_id`, `product_id`, `product_name`, `qty`, `price`, `options`) VALUES
(6, 8, 6, 'Samsung The New QLED TV', 2, 8500000, ''),
(7, 8, 7, 'Monitor 4K', 1, 5350000, ''),
(8, 8, 8, 'Laptop Toshiba Intel i5', 1, 4320000, ''),
(9, 8, 9, 'Laptop Macbook Pro 2018 Touchbar 256 GB', 1, 32000000, ''),
(10, 9, 9, 'Laptop Macbook Pro 2018 Touchbar 256 GB', 1, 32000000, ''),
(11, 10, 8, 'Laptop Toshiba Intel i5', 1, 4320000, ''),
(12, 11, 6, 'Samsung The New QLED TV', 2, 8500000, ''),
(13, 11, 9, 'Laptop Macbook Pro 2018 Touchbar 256 GB', 1, 32000000, ''),
(14, 12, 7, 'Monitor 4K', 1, 5350000, ''),
(15, 14, 9, 'Laptop Macbook Pro 2018 Touchbar 256 GB', 1, 32000000, ''),
(16, 14, 8, 'Laptop Toshiba Intel i5', 1, 4320000, ''),
(17, 15, 6, 'Samsung The New QLED TV', 1, 8500000, ''),
(18, 16, 7, 'Monitor 4K', 1, 5350000, ''),
(19, 16, 8, 'Laptop Toshiba Intel i5', 1, 4320000, ''),
(20, 17, 6, 'Samsung The New QLED TV', 1, 8500000, ''),
(21, 50, 1, 'UHD 4K Smart TV', 1, 10250000, ''),
(22, 51, 1, 'UHD 4K Smart TV', 1, 10250000, ''),
(23, 62, 6, 'Samsung New QLED', 1, 8500000, ''),
(24, 64, 1, 'UHD 4K Smart TV', 1, 10250000, ''),
(25, 65, 1, 'UHD 4K Smart TV', 1, 10250000, ''),
(26, 66, 1, 'UHD 4K Smart TV', 1, 10250000, ''),
(27, 74, 1, 'UHD 4K Smart TV', 1, 10250000, ''),
(28, 78, 7, 'Monitor 4K', 1, 5350000, ''),
(29, 79, 1, 'UHD 4K Smart TV', 1, 10250000, ''),
(30, 79, 6, 'Samsung New QLED', 1, 8500000, ''),
(31, 80, 7, 'Monitor 4K', 1, 5350000, ''),
(32, 81, 7, 'Monitor 4K', 1, 5350000, ''),
(33, 82, 8, 'Toshiba Intel i5', 1, 4320000, ''),
(34, 83, 6, 'Samsung New QLED', 1, 8500000, ''),
(35, 84, 6, 'Samsung New QLED', 1, 8500000, ''),
(36, 85, 6, 'Samsung New QLED', 1, 8500000, ''),
(37, 85, 7, 'Monitor 4K', 1, 5350000, ''),
(38, 86, 1, 'UHD 4K Smart TV', 1, 10250000, ''),
(39, 87, 7, 'Monitor 4K', 1, 5350000, ''),
(40, 88, 7, 'Monitor 4K', 1, 5350000, ''),
(41, 89, 9, 'MacBook Pro Touchbar', 1, 32000000, ''),
(42, 90, 14, 'Samsung S9', 1, 10399000, ''),
(43, 91, 16, 'Logitech G933 Wireless Gaming', 1, 1715000, ''),
(44, 92, 16, 'Logitech G933 Wireless Gaming', 1, 1715000, ''),
(45, 92, 17, 'Arduino UNO', 1, 85000, ''),
(46, 93, 7, 'Monitor 4K', 1, 5350000, ''),
(47, 94, 1, 'UHD 4K Smart TV', 1, 10250000, ''),
(48, 95, 15, 'Asus ROG Phone', 1, 14000000, ''),
(49, 96, 7, 'Monitor 4K', 1, 5350000, ''),
(50, 97, 7, 'Monitor 4K', 1, 5350000, ''),
(51, 99, 7, 'Monitor 4K', 1, 5350000, ''),
(52, 100, 6, 'Samsung New QLED', 1, 8500000, ''),
(53, 101, 1, 'UHD 4K Smart TV', 2, 10250000, ''),
(54, 102, 17, 'Arduino UNO', 1, 85000, ''),
(55, 103, 14, 'Samsung S9', 1, 10399000, ''),
(56, 104, 6, 'Samsung New QLED', 1, 8500000, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_produk`
--

CREATE TABLE `tabel_produk` (
  `id_barang` int(11) NOT NULL,
  `nama_barang` varchar(255) NOT NULL,
  `merk` varchar(255) NOT NULL,
  `kategori` varchar(255) NOT NULL,
  `jumlah` int(100) NOT NULL,
  `harga` int(100) NOT NULL,
  `garansi` varchar(255) NOT NULL,
  `gambar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tabel_produk`
--

INSERT INTO `tabel_produk` (`id_barang`, `nama_barang`, `merk`, `kategori`, `jumlah`, `harga`, `garansi`, `gambar`) VALUES
(1, 'UHD 4K Smart TV', 'Samsung', 'TV', 2, 10250000, '3 Tahun', 'samsungTV.jpg'),
(6, 'Samsung New QLED', 'Samsung', 'TV', 0, 8500000, '2 Tahun', 'tv.jpg'),
(7, 'Monitor 4K', 'LG', 'Monitor', 12, 5350000, '10 Bulan', 'monitor.jpg'),
(8, 'Toshiba Intel i5', 'Toshiba', 'Laptop', 20, 4320000, '1 Tahun', 'laptop.jpg'),
(9, 'MacBook Pro Touchbar', 'Apple', 'Laptop', 5, 32000000, '1 Tahun', 'macbook.jpeg'),
(13, 'Asus ROG Zephyrus', 'Asus', 'Laptop', 10, 31599000, '2 Tahun', 'rog_zephyrus.jpg'),
(14, 'Samsung S9', 'Samsung', 'Smartphone', 8, 10399000, '1 Tahun', 'samsungS9.jpg'),
(15, 'Asus ROG Phone', 'Asus', 'Smartphone', 3, 14000000, '1 Tahun', 'rog_phone.jpg'),
(16, 'Logitech G933 Wireless Gaming', 'Logitech', 'Headphone', 8, 1715000, '2 Tahun', 'headphone logitech.jpg'),
(17, 'Arduino UNO', 'Arduiono', 'Microcontroller', 2, 85000, '6 Bulan', 'arduino.jpg'),
(18, 'tes', 'tes', 'tes', 2, 124142, '3 Tahun', 'macbook.jpeg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabel_produk`
--
ALTER TABLE `tabel_produk`
  ADD PRIMARY KEY (`id_barang`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `tabel_produk`
--
ALTER TABLE `tabel_produk`
  MODIFY `id_barang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
