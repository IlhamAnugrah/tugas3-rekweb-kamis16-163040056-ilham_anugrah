<?php 

class Barang_model extends CI_Model {

    public function __construct()
	{
		parent::__construct();
	}

    public function getData( $id = null ) {
        $this->db->select("*");
        $this->db->from("tabel_produk");

        if ( $id == null ) {
            $this->db->order_by('id_barang', 'asc');
        } else {
            $this->db->where('id_barang', $id);
        }
        return $this->db->get();
    }

    public function cari( $cari ) {
        $this->db->select("*");
        $this->db->from("tabel_produk");
        $this->db->like('nama_barang', $cari);
        $this->db->or_like('merk', $cari);
        $this->db->or_like('kategori', $cari);
        $this->db->or_like('jumlah', $cari);
        $this->db->or_like('harga', $cari);
        $this->db->or_like('garansi', $cari);

        return $this->db->get();
    }

    public function insert( $data ) {
       $this->db->insert('tabel_produk', $data);
       return $this->db->affected_rows();
    }

    public function update( $table, $data, $par, $var ) {
        $this->db->update( $table, $data, array($par => $var) );
        return $this->db->affected_rows();
    }
    
    public function delete( $table, $par, $var ) {
       $this->db->where($par, $var);
       $this->db->delete($table);
       return $this->db->affected_rows();
    }

}