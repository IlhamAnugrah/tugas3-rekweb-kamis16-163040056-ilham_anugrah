<?php

require APPPATH . '/libraries/REST_Controller.php';

class Barang extends REST_Controller {
    
    public function __construct( $config = "rest" ) {
        parent::__construct($config);
        $this->load->model("Barang_model", "tabel_produk");
    }

    public function index_get() {
        $id = $this->get('id_barang');
        $cari = $this->get('cari');

        if ( $cari != "" ) {
            $barang = $this->tabel_produk->cari($cari)->result();
        } else if ( $id == '' ) {
            $barang = $this->tabel_produk->getData(null)->result();
        } else {
            $barang = $this->tabel_produk->getData($id)->result();
        }
        $this->response($barang);
    }

    public function index_put() {
        $id = $this->put('id_barang');
        $nama_barang = $this->put('nama_barang');
        $merk = $this->put('merk');
        $kategori = $this->put('kategori');
        $jumlah = $this->put('jumlah');
        $harga = $this->put('harga');
        $garansi = $this->put('garansi');
        $gambar = $this->put('gambar');

        $data = array(
            'nama_barang'  => $nama_barang,
            'merk'         => $merk,
            'kategori'     => $kategori,
            'jumlah'       => $jumlah,
            'harga'        => $harga,
            'garansi'      => $garansi,
            'gambar'       => $gambar
        );
        
        $update = $this->tabel_produk->update('tabel_produk', $data, 'id_barang', $this->put('id_barang'));

        if ( $update ) {
            $this->response(array('status' => 'success', 200));
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    public function index_post() {
        $nama_barang = $this->post('nama_barang');
        $merk = $this->post('merk');
        $kategori = $this->post('kategori');
        $jumlah = $this->post('jumlah');
        $harga = $this->post('harga');
        $garansi = $this->post('garansi');
        $gambar = $this->post('gambar');

        $data = array(
            'nama_barang'  => $nama_barang,
            'merk'         => $merk,
            'kategori'     => $kategori,
            'jumlah'       => $jumlah,
            'harga'        => $harga,
            'garansi'      => $garansi,
            'gambar'       => $gambar
        );

        $insert = $this->tabel_produk->insert($data);
        if ( $insert ) {
            $this->response(array('status' => 'success', 200));
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    public function index_delete() {

       $id = $this->delete('id_barang');

       $delete = $this->tabel_produk->delete('tabel_produk', 'id_barang', $id);
        if ( $delete ) {
            $this->response(array('status' => 'success', 200));
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
}